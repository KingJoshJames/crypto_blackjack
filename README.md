# Crypto BlackJack
Crypto BlackJack implemented in python

## Dependencies required: pip, progress, click, simple-crypt

If you haven't already installed pip, visit the following link to download: https://pip.pypa.io/en/stable/installing/ - You can thank me later. 😎

To install dependencies run each of the following commands. You may need to precede each command w/ sudo to bypass access permissions.
```bash
pip install progress
```
```bash
pip install click
```
```bash
pip install simple-crypt
```

## Installation
1. Open Terminal.

2. Change the current working directory to the location where you want the cloned directory to be made.

3. Type or copy the following command and press 'enter'. Your local clone will be created.

```bash
git clone https://github.com/KingJoshJames/BlackJack.git
```
4. Type the following command to change directory to BlackJack.
```bash
cd blackjack
```
5. While in the BlackJack directory of the clone, type or copy the following and hit 'enter'.

```bash
python CryptoBlackJack.py
```

## Design Choices

I utilized the simple-crypt package to encrypt and decrypt the dealer's down-facing card to hide it from the player until the final hands are revealed.
Simple-crypt uses AES256 and includes a check (an HMAC with SHA256) to warn when ciphertext data are modified.

The dealer must stand on a 17 or better and hit on a 16 or under.

An ace is counted as a 1 if the total value of the hand is >= 11.

When the dealer and the player both have hands that have the same value its called a push. Initial bet returned to player.

All other rules are pretty self-explanatory.

## Enjoy!

P.S. The Ethereum address in the game is mine if you're feeling generous. 😏