# Rules of the game

# When the dealer and the player both have hands that have the same
# value its called a push. Initial bet returned to player.

# The dealer must stand on a 17 or better and hit on a 16 or under

# An ace is counted as a 1 if the total value of the hand is >= 11

# One of the dealer's cards is shown while the other is encypted until the final results are shown. Then it is decrypted. 


# future additions (**not currently in game**)
# add option to double-down on bet before final hands are revealed
# add insurance | insurance == bet_amount / 2. Ask, "Would you like insurance?" 
# when the dealers up card is an ace. If the dealer does not have blackjack, player loses insurance.
# if dealer does have blackjack, player may lose bet, but insurance * 3 = reward
# i.e. Player bet 100. Dealer's up card is an ace. Player takes insurance for 50. Dealer has 
#blackjack. Player loses 100 bet. Player receives 150 in insurance. 

import time
from time import sleep
import random
from progress.bar import Bar
import click 
from simplecrypt import encrypt, decrypt
name = raw_input("What is your name? ")
sleep(1); print('Hello ' + name + '!') 
sleep(1); print ('Welcome to Crypto BlackJack')
sleep(1); print('How many decks would you like to play with? ')
num_decks = raw_input("Type either 1, 2, 4, 6, or 8 and press 'Enter' ")
sleep(1); print('Shuffling ' + num_decks + ' deck(s)...')
cards_in_deck = 52
total_cards = cards_in_deck * int(num_decks)
sleep(1); print("Total Cards: " + str(total_cards))
sleep(1.25); bar = Bar('Processing ETH transfer', max=10)
for i in range(10):
    sleep(.4); print(' Transferring from ' + name + "'s ETH Wallet 0x19c058a4fBBD0192C403BEd9549342b4E25743C1")
    bar.next()
bar.finish()
bank_balance = 1000000
sleep(1); print('ETH transfer complete')
sleep(1); print('Your ETH Balance:  $' + str(bank_balance))
sleep(1); print('Woah! We\'ve got a baller at the table')

values = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']
suits = ['Hearts', 'Spades', 'Diamonds', 'Clubs']

def create_deck(values, suits):
    deck = []
    for x in values:
        for y in suits:
            deck.append((x, y))
    return deck

deck_of_cards = create_deck(values, suits)

deck1 = []
for i in range(0,52,1):
    s = ' of '
    deck1.append(s.join(deck_of_cards[i]))
deck1 = deck1 * int(num_decks)

def deal(deck):
    hand = []
    for i in range(2):
        card = deck.pop()
        hand.append(card)
    return hand

def total_value(hand):
    total_value = 0
    for card in hand:
        if card[0] == 'J' or card[0] == 'Q' or card[0] == 'K' or card[0:2] == '10':
            total_value +=10
        elif card[0] == 'A':
            if total_value >= 11: total_value += 1
            else: 
                total_value += 11
        else: 
            total_value += int(card[0])
    return total_value

def bet(amount, balance):
    balance -= amount
    pot = 2 * amount
    print('Total Pot: $' + str(pot))
    return pot

def encrypt_bar(hand):
    fill_char = click.style('$', fg='blue') 
    with click.progressbar(range(100), label='Encrypting Dealer\'s Hand...', fill_char=fill_char) as bar:
        for i in bar:
            sleep(0.02)
    ciphertext = encrypt('CryptoIsKing', hand)
    return ciphertext

def decrypt_bar(ciphertext):
    fill_char = click.style('$', fg='blue')
    with click.progressbar(range(100), label='Decrypting Dealer\'s Hand...', fill_char=fill_char) as bar:
        for i in bar:
            sleep(0.02)
    plaintext = decrypt('CryptoIsKing', ciphertext)

def win_pot(total_pot, updated_balance): 
    updated_balance += total_pot
    print('Huge Win!!! New ETH balance: $' + str(updated_balance))
    return updated_balance

def lose_pot(total_pot, updated_balance): 
    print('The House Always Wins :-( Updated ETH balance: ' + str(updated_balance))
    return updated_balance

def push_pot(total_pot, updated_balance): 
    updated_balance += (total_pot/2)
    print('Bet amount returned due to tie. Updated ETH balance: ' + str(updated_balance))
    return updated_balance

def print_final_hands(players_hand, dealers_hand, ciphertext):
    sleep(0.5); print("Your Hand: " + str(players_hand) + " Total Value of your hand: " + str(total_value(players_hand)))
    decrypt_bar(ciphertext)
    sleep(0.5); print("Dealer's hand: " + str(dealers_hand) + " Total Value of Dealer's hand: " + str(total_value(dealers_hand)))

def score(players_hand, dealers_hand, total_pot, updated_balance, ciphertext):
    if total_value(players_hand) == 21:
        print_final_hands(players_hand, dealers_hand, ciphertext)
        sleep(0.5); print("Congratulations, BlackJack!")
        final_balance = win_pot(total_pot, updated_balance)
        next_game(final_balance)
    elif total_value(dealers_hand) == 21:
        print_final_hands(players_hand, dealers_hand, ciphertext)
        sleep(0.5); print("You lost, the dealer got a BlackJack.")
        final_balance = lose_pot(total_pot, updated_balance)
        next_game(final_balance)
    elif total_value(players_hand) > 21:
        print_final_hands(players_hand, dealers_hand, ciphertext)
        sleep(0.5); print("You busted, you lose.")
        final_balance = lose_pot(total_pot, updated_balance)
        next_game(final_balance)
    elif total_value(dealers_hand) > 21:
        print_final_hands(players_hand, dealers_hand,ciphertext)
        sleep(0.5); print("Dealer busted, you win!")
        final_balance = win_pot(total_pot, updated_balance)
        next_game(final_balance)
    elif total_value(players_hand) < total_value(dealers_hand):
        print_final_hands(players_hand, dealers_hand, ciphertext)
        sleep(0.5); print("You lose, your score is lower than the dealers.")
        final_balance = lose_pot(total_pot, updated_balance)
        next_game(final_balance)
    elif total_value(players_hand) > total_value(dealers_hand):
        print_final_hands(players_hand, dealers_hand, ciphertext)
        sleep(0.5); print("You win! Your score is higher than the dealers.")
        final_balance = win_pot(total_pot, updated_balance)
        next_game(final_balance)
    elif total_value(players_hand) == total_value(dealers_hand):
        print_final_hands(players_hand, dealers_hand, ciphertext)
        sleep(0.5); print("Push! It's a tie.")
        final_balance = push_pot(total_pot, updated_balance)
        next_game(final_balance)

def hit(hand):
    card = deck1.pop()
    hand.append(card)
    return hand

def next_game(balance):
    print("Do you want to play again or cash out?")
    answer = raw_input("Type 'P' to play again or '$' to cash out ").upper()
    if answer == 'P':
        players_hand = []
        dealers_hand = []
        pot = []
        game(balance)
    elif answer == '$': 
        print('Take the money and run!')
        bar = Bar('Processing ETH transfer', max=10)
        for i in range(10):
            sleep(1.25); print ' Transferring $' + str(balance) + ' to ' + name + "'s ETH Wallet 0x19c058a4fBBD0192C403BEd9549342b4E25743C1"
            bar.next()
        bar.finish()
        sleep(1); print('ETH transfer complete') 

def game(start_balance):
    print('Starting ETH Balance: $' + str(start_balance))
    random.shuffle(deck1)
    for i in range(3):
        sleep(.75); print('Shuffling...')
    sleep(1); print("Everyday I\'m Shufflin'")
    sleep(1); print('How much do you want to bet?')
    bet_amount = int(raw_input('Type any amount <= $' + str(start_balance) + ': $'))
    total_pot = bet(bet_amount, start_balance)
    updated_balance = start_balance - bet_amount
    print('Your current ETH Balance: $' + str(updated_balance))
    players_hand = deal(deck1)
    sleep(2); print('Your hand: '); print(players_hand)
    sleep(1); print('Total value of your hand: '); print(total_value(players_hand))    
    dealers_hand = deal(deck1)
    sleep(1); cipher = encrypt_bar(dealers_hand[1])
    sleep(2); print("Dealer's hand: "); print(dealers_hand[0], cipher)
    sleep(1); print("Would you like to hit or stand?")
    action = raw_input("Type H for hit & S for stand ").upper()
    if action == 'H':
        hit(players_hand)
        print('Your hand: '); print(players_hand)
        print('Total value of your hand: '); print(total_value(players_hand))
        if total_value(players_hand) > 21:
            score(players_hand, dealers_hand, total_pot, updated_balance, cipher)
        if total_value(players_hand) == 21:
            score(players_hand, dealers_hand, total_pot, updated_balance, cipher)
        while total_value(players_hand) < 21:
            sleep(0.5); print("Would you like to hit or stand?")
            action = raw_input("Type H for hit & S for stand ").upper()
            if action == "H":
                hit(players_hand)
                print('Your hand: '); print(players_hand)
                print('Total value of your hand: '); print(total_value(players_hand))
                if total_value(players_hand) > 21:
                    score(players_hand, dealers_hand, total_pot, updated_balance, cipher)
                elif total_value(players_hand) == 21:
                    score(players_hand, dealers_hand, total_pot, updated_balance, cipher)
            elif action == "S":
                while total_value(dealers_hand) < 17:
                    hit(dealers_hand)
                score(players_hand, dealers_hand, total_pot, updated_balance, cipher)
                break
    elif action == 'S':
        while total_value(dealers_hand) < 17:
            hit(dealers_hand)
        score(players_hand, dealers_hand, total_pot, updated_balance, cipher)

game(bank_balance)



